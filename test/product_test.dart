import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hello_world/utils/product.dart';
import 'package:hello_world/widgets/product.dart';
import 'package:hello_world/widgets/product_list.dart';

void main() {
  testWidgets('product listing widget testing', (WidgetTester tester) async {
    var items = [
      Product(
        name: "iPhone",
        description: "iPhone is the stylist phone ver",
        price: 1000,
        image: "iphone.jpg",
      ),
      Product(
        name: "iPhone X",
        description: "iPhone X is the stylist phone ver",
        price: 1000,
        image: "iphone.jpg",
      ),
    ];
    var _controler =
        AnimationController(vsync: tester, duration: Duration(seconds: 1));
    await tester.pumpWidget(Directionality(
        textDirection: TextDirection.ltr,
        child: ProductList(
          items: items,
          animation: _controler,
        )));

    expect(find.byType(ProductBox), findsWidgets);
    expect(find.text("iPhone X"), findsOneWidget);
    expect(find.text("iPhone"), findsWidgets);
  });
}
