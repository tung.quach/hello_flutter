import "package:flutter/material.dart";
import "package:hello_world/utils/product.dart";
import "package:hello_world/widgets/rating_box.dart";
import 'package:url_launcher/url_launcher.dart';

class ProductView extends StatelessWidget {
  ProductView({Key key, this.item}) : super(key: key);
  final Product item;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this.item.name),
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Image.asset(
                "assets/images/" + item.image,
                fit: BoxFit.fill,
                width: MediaQuery.of(context).size.width,
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.fromLTRB(2.0, 10.0, 2.0, 10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        item.name,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(item.description),
                      Text("Price: " + item.price.toString()),
                      RatingBox(),
                      RaisedButton(
                        onPressed: _openBrowser,
                        child: Text("Click for help"),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<bool> _openBrowser() async {
    const url = 'https://flutter.dev';
    if (await canLaunch(url)) {
      return await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
