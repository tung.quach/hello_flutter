import 'package:flutter/material.dart';
import 'package:hello_world/db/db.dart';
import 'package:hello_world/utils/product.dart';
import 'package:hello_world/widgets/product_list.dart';

class HomeView extends StatefulWidget {
  HomeView({Key key, this.title, this.animation}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;
  final Animation<double> animation;

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final Future<List<Product>> items = SQLiteDBProvider.db.getAllProducts();

  _HomeViewState();

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the HomeView object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: FutureBuilder<List<Product>>(
        future: items,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text(snapshot.error);
          }

          return snapshot.hasData
              ? ProductList(items: snapshot.data, animation: widget.animation)
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
