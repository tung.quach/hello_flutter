import 'package:flutter/material.dart';
import 'package:hello_world/utils/animated.dart';
import 'package:hello_world/utils/product.dart';
import 'package:hello_world/views/product_view.dart';
import 'package:hello_world/widgets/product.dart';

class ProductList extends StatelessWidget {
  final List<Product> items;
  final Animation<double> animation;

  ProductList({Key key, this.items, this.animation}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      padding: EdgeInsets.fromLTRB(2.0, 10.0, 2.0, 10.0),
      itemCount: items.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          child: MyAnimated(
            child: ProductBox(
              item: items[index],
            ),
            animation: animation,
          ),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ProductView(item: items[index]),
              ),
            );
          },
        );
      },
    );
  }
}
