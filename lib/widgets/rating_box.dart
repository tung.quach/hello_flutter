import 'package:flutter/material.dart';

class RatingBox extends StatefulWidget {
  @override
  _RatingBoxState createState() => _RatingBoxState();
}

class _RatingBoxState extends State<RatingBox> {
  int _rating = 0;

  _setRating(int score) {
    setState(() {
      _rating = score;
    });
  }

  @override
  Widget build(BuildContext context) {
    double _size = 20;

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: _rating >= 1 ? Icon(Icons.star) : Icon(Icons.star_border),
            color: Colors.red[500],
            iconSize: _size,
            onPressed: () {
              _setRating(1);
            },
          ),
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: _rating >= 2 ? Icon(Icons.star) : Icon(Icons.star_border),
            color: Colors.red[500],
            iconSize: _size,
            onPressed: () {
              _setRating(2);
            },
          ),
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: _rating >= 3 ? Icon(Icons.star) : Icon(Icons.star_border),
            color: Colors.red[500],
            iconSize: _size,
            onPressed: () {
              _setRating(3);
            },
          ),
        ),
      ],
    );
  }
}
