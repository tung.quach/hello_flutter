import 'package:flutter/material.dart';
import 'package:hello_world/utils/product.dart';
import 'package:hello_world/widgets/rating_box.dart';

class ProductBox extends StatelessWidget {
  ProductBox({Key key, this.item}) : super(key: key);

  final Product item;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(2),
      height: 140,
      child: Card(
        child: Row(
          children: <Widget>[
            Image.asset("assets/images/" + item.image),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(5),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text(
                      item.name,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text(
                      item.description,
                      textAlign: TextAlign.center,
                    ),
                    Text("Price: " + item.price.toString()),
                    RatingBox()
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
