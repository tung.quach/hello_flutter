import 'dart:convert';
import 'package:http/http.dart' as http;

class Product {
  int id;
  final String name;
  final String description;
  final int price;
  final String image;
  static final columns = ["id", "name", "description", "price", "image"];

  Product({this.id, this.name, this.description, this.price, this.image});

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "description": description,
        "price": price,
        "image": image,
      };

  factory Product.fromJson(Map<String, dynamic> data) {
    return Product(
      name: data["name"],
      description: data["description"],
      price: data["price"],
      image: data["image"],
    );
  }

  factory Product.fromMap(Map<String, dynamic> data) {
    return Product(
      id: data["id"],
      name: data["name"],
      description: data["description"],
      price: data["price"],
      image: data["image"],
    );
  }

  List<Product> parseProducts(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Product>((json) => Product.fromJson(json)).toList();
  }

  Future<List<Product>> fetchProducts() async {
    final res =
        await http.get('http://www.mocky.io/v2/5e7c59d93500007d000689d8');
    if (res.statusCode == 200) {
      return parseProducts(res.body);
    } else {
      throw Exception('Unable to get products from REST API');
    }
  }

  static List<Product> getProducts() {
    List<Product> items = <Product>[];

    items.add(Product(
      name: "iPhone",
      description: "iPhone is the stylist phone ver",
      price: 1000,
      image: "iphone.jpg",
    ));

    items.add(Product(
      name: "Pixel",
      description: "Pixel is the most featureful phone ever",
      price: 800,
      image: "pixel.jpg",
    ));

    items.add(Product(
      name: "Laptop",
      description: "Laptop is most productive development tool",
      price: 2000,
      image: "laptop.jpg",
    ));

    items.add(Product(
      name: "Tablet",
      description: "Tablet is the most useful device ever for meeting",
      price: 1500,
      image: "tablet.jpg",
    ));

    items.add(Product(
      name: "Pendrive",
      description: "Pendrive is useful storage medium",
      price: 100,
      image: "pendrive.jpg",
    ));

    items.add(Product(
      name: "Floppy Drive",
      description: "Floppy drive is useful rescue storage medium",
      price: 20,
      image: "floppy.jpg",
    ));

    return items;
  }
}
