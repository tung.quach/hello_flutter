import 'package:flutter/material.dart';

class MyAnimated extends StatelessWidget {
  MyAnimated({this.child, this.animation});

  final Widget child;
  final Animation<double> animation;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AnimatedBuilder(
        animation: animation,
        child: child,
        builder: (context, child) {
          return Container(
            child: Opacity(opacity: animation.value, child: child),
          );
        },
      ),
    );
  }
}
