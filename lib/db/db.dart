import 'dart:io';
import 'package:hello_world/utils/product.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class SQLiteDBProvider {
  SQLiteDBProvider._();
  static final SQLiteDBProvider db = SQLiteDBProvider._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, 'lib/db/product_table.db');

    return await openDatabase(
      path,
      version: 1,
      onOpen: (db) {},
      onCreate: (Database db, int version) async {
        await db.execute(
            "CREATE TABLE products(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, description TEXT, price INTEGER, image TEXT)");
        await db.execute(
            "INSERT INTO products ('name', 'description', 'price', 'image') values (?, ?, ?, ?)",
            ["iPhone", "iPhone is the stylist phone ever", 1000, "iphone.jpg"]);
      },
    );
  }

  Future<List<Product>> getAllProducts() async {
    final db = await database;
    List<Map> results = await db.query("products",
        columns: Product.columns, orderBy: "id DESC");

    List<Product> products = new List();

    results.forEach((result) {
      Product product = Product.fromMap(result);
      products.add(product);
    });

    return products;
  }

  Future<Product> getProductByID(int id) async {
    final db = await database;
    var result = await db.query("products",
        columns: Product.columns, where: "id = ?", whereArgs: [id]);
    return result.isNotEmpty ? Product.fromMap(result.first) : null;
  }

  Future<Product> insert(Product product) async {
    final db = await database;
    product.id = await db.insert("products", product.toMap());
    return product;
  }

  Future<int> update(Product product) async {
    final db = await database;
    return await db.update("products", product.toMap(),
        where: "id = ?", whereArgs: [product.id]);
  }

  Future<int> delete(int id) async {
    final db = await database;
    return await db.delete("products", where: "id = ?", whereArgs: [id]);
  }
}
